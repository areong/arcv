#ifndef ARCV_HOMOGRAPHY_H_
#define ARCV_HOMOGRAPHY_H_

#include <Eigen/Dense>

#include <array>

namespace arcv
{

/**
 * Solve a homography that transforms between 2D projective spaces (P2) by
 * providing four pairs of points in 2D Euclidean spaces (R2).
 * The homography is represented as a 3x3 matrix.
 * @param[in] src_points an array of 2D points that are mapped to dst_points
 *  by the homography.
 * @param[in] dst_points an array of 2D points that are mapped from src_points
 *  by the homography.
 * @return the homography that maps the ith src point to the ith dst point.
 * @throw if any three input points are collinear.
 */
auto solve_homography(std::array<Eigen::Vector2d, 4> const& src_points,
                      std::array<Eigen::Vector2d, 4> const& dst_points) -> Eigen::Matrix3d;

/**
 * Check if the input three points are collinear, i.e. lying on the same line.
 * Assume the input points are in an R2 space.
 * @param[in] point_0 a point in R2.
 * @param[in] point_1 a point in R2.
 * @param[in] point_2 a point in R2.
 * @return if the input three points lie on the same line.
 */
auto check_collinearity(Eigen::Vector2d const& point_0, Eigen::Vector2d const& point_1,
                        Eigen::Vector2d const& point_2) -> bool;

/**
 * Normalize the range of the input points to x: [-1, 1], y: [-1, 1].
 * @param[in] points points in R2.
 * @return normalized points.
 */
auto normalize_range(std::vector<Eigen::Vector2d> const& points) -> std::vector<Eigen::Vector2d>;

} // namespace arcv

#endif // ARCV_HOMOGRAPHY_H_
