#include "arcv/homography.h"

#include <algorithm>
#include <exception>

namespace arcv
{

auto solve_homography(std::array<Eigen::Vector2d, 4> const& src_points,
                      std::array<Eigen::Vector2d, 4> const& dst_points) -> Eigen::Matrix3d
{
    // Summary:
    //  Solve linear equations such that the resulting homography satisfies
    //  {dst_point, 1} = homography * {src_point, 1}.
    //
    // Notation:
    //  s_i: the row i component of a source point
    //  d_i: the row i component of a destination point
    //  H_i,j: the row i column j component of the homography
    //
    // Each pair of points form two linear equations:
    //  d_0 = (H_0,0 * s_0 + H_0,1 * s_1 + H_0,2 * 1) / (H_2,0 * s_0 + H_2,1 * s_1 + H_2,2 * 1)
    //  d_1 = (H_1,0 * s_0 + H_1,1 * s_1 + H_1,2 * 1) / (H_2,0 * s_0 + H_2,1 * s_1 + H_2,2 * 1)
    //
    // Which become:
    //  s_0 * H_0,0 + s_1 * H_0,1 + H_0,2 - s_0 * d_0 * H_2,0 - s_1 * d_0 * H_2,1 - d_0 * H_2,2 = 0
    //  s_0 * H_1,0 + s_1 * H_1,1 + H_1,2 - s_0 * d_1 * H_2,0 - s_1 * d_1 * H_2,1 - d_1 * H_2,2 = 0
    //
    // There are totally 8 equations, solving the 9 coefficients of the 3x3
    // homography matrix up to a scale.

    // Construct the linear system Ax = b.
    // Note that b is a zero vector.
    Eigen::Matrix<double, 8, 9> A;
    auto i_row = 0;
    for (auto i_point = 0; i_point < src_points.size(); ++i_point)
    {
        auto const& src_point = src_points[i_point];
        auto const& dst_point = dst_points[i_point];

        // Set two rows of A.
        A.row(i_row++) << src_point(0), src_point(1), 1, 0, 0, 0, -1 * src_point(0) * dst_point(0),
            -1 * src_point(1) * dst_point(0), -1 * dst_point(0);
        A.row(i_row++) << 0, 0, 0, src_point(0), src_point(1), 1, -1 * src_point(0) * dst_point(1),
            -1 * src_point(1) * dst_point(1), -1 * dst_point(1);
    }

    // Solve the null space of A and represent it as N (the column space of N
    // is the null space of A).
    Eigen::MatrixXd const N = A.fullPivLu().kernel();

    // The nullity of A should be 1 for valid input.
    if (N.cols() != 1)
    {
        throw std::invalid_argument("There may be three input points that are collinear.");
    }

    // Output the homography.
    Eigen::Matrix3d H;
    H << N.block<3, 1>(0, 0).transpose(), N.block<3, 1>(3, 0).transpose(),
        N.block<3, 1>(6, 0).transpose();
    return H;
}

auto check_collinearity(Eigen::Vector2d const& point_0, Eigen::Vector2d const& point_1,
                        Eigen::Vector2d const& point_2) -> bool
{
    // Normalize the range of the input point to x: [-1, 1], y: [-1, 1]
    // such that a constant tolerance value can be applied to any input points.
    // (The tolerance is used below at function returning.)
    auto const normalized_points = normalize_range({point_0, point_1, point_2});

    // Convert the input points to homogeneous coordinates.
    Eigen::Vector3d p_0, p_1, p_2;
    p_0 << normalized_points[0], 1;
    p_1 << normalized_points[1], 1;
    p_2 << normalized_points[2], 1;

    // Compute the volume of the parallelepiped formed by the three vectors.
    // The computation is also called a scalar triple product.
    auto volume = p_0.cross(p_1).dot(p_2);

    // Scalar triple product can be negative. Take absolute value.
    volume *= volume < 0 ? -1 : 1;

    // If the volume is smaller than a tolerance 1e-8, regard the three input
    // points collinear.
    // The tolerance of 1e-8 is tested by 1e7 sets of random input points.
    // (Please check the corresponding test code.)
    return volume < 1e-8;
}

auto normalize_range(std::vector<Eigen::Vector2d> const& points) -> std::vector<Eigen::Vector2d>
{
    // Prepare the output points.
    std::vector<Eigen::Vector2d> output_points(points.size());

    if (points.empty())
    {
        return output_points;
    }
    else if (points.size() == 1)
    {
        // Translate the only point to the origin.
        output_points[0] << 0, 0;
        return output_points;
    }

    // Find the x y range of the input points.
    auto const [min_x_point_iterator, max_x_point_iterator] = std::minmax_element(
        points.begin(), points.end(),
        [](auto const& point_0, auto const& point_1) { return point_0(0) < point_1(0); });
    auto const [min_y_point_iterator, max_y_point_iterator] = std::minmax_element(
        points.begin(), points.end(),
        [](auto const& point_0, auto const& point_1) { return point_0(1) < point_1(1); });
    auto x_min = (*min_x_point_iterator)(0);
    auto x_max = (*max_x_point_iterator)(0);
    auto y_min = (*min_y_point_iterator)(1);
    auto y_max = (*max_y_point_iterator)(1);

    // If the size of x range or y range is zero, extend the size to 2
    // to avoid dividing by zero in the next step.
    if (x_min == x_max)
    {
        x_min -= 1;
        x_max += 1;
    }
    if (y_min == y_max)
    {
        y_min -= 1;
        y_max += 1;
    }

    // Set the scale matrix such that the final range is x: [-1, 1], y:[-1, 1].
    auto const x_scale = 2 / (x_max - x_min);
    auto const y_scale = 2 / (y_max - y_min);
    Eigen::Matrix2d scale;
    scale << x_scale, 0, 0, y_scale;

    // Set the translation vector such that the final center point is at the origin.
    Eigen::Vector2d t;
    t << -0.5 * (x_max + x_min), -0.5 * (y_max + y_min);

    // Construct the transformation matrix by the equation
    // x' = S (x + t), where the x is an input R2 vector, t is the translation
    // vector, and S is the scale matrix.
    Eigen::Matrix3d transformation;
    transformation << scale, scale * t, Eigen::Vector2d::Zero().transpose(), 1;

    // Tranform the input points.
    for (int i = 0; i < points.size(); ++i)
    {
        auto const& input_point = points[i];
        auto& output_point = output_points[i];

        // Convert to homogeneous coordinates.
        Eigen::Vector3d homogeneous_point(input_point(0), input_point(1), 1);

        // Transform the input point.
        homogeneous_point = transformation * homogeneous_point;

        // Assign to the output point.
        output_point << homogeneous_point(0), homogeneous_point(1);
    }

    return output_points;
}

} // namespace arcv
