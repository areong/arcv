cmake_minimum_required(VERSION 3.12)
project(arcv LANGUAGES CXX)

# Please install Eigen3 from source by CMake such that imported targets are available.
# Do not install Eigen3 packages by apt.
find_package(Eigen3)

# Please install Boost from source such that CMake can find Boost correctly.
# Do not install Boost packages by apt.
find_package(Boost REQUIRED COMPONENTS unit_test_framework)

# Enable tests.
include(CTest)

set(CMAKE_COMPILE_WARNING_AS_ERROR ON)

set(PROJECT_INCLUDE_DIR ${PROJECT_SOURCE_DIR}/include)

add_subdirectory(src/arcv)
add_subdirectory(tests/src/arcv)
