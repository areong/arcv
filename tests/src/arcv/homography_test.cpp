#define BOOST_TEST_MODULE Homography Test
#define BOOST_TEST_DYN_LINK

#include "arcv/homography_test.h"
#include "arcv/homography.h"

#include <Eigen/Dense>
#include <Eigen/Geometry>

#include <boost/test/data/monomorphic.hpp>
#include <boost/test/data/test_case.hpp>
#include <boost/test/unit_test.hpp>

#include <algorithm>
#include <array>
#include <exception>
#include <iostream>
#include <random>
#include <utility>
#include <vector>

#include <cmath>

namespace arcv
{

namespace test
{

auto scale_from_range_0_1(double const value, double const lower, double const upper) -> double
{
    return (upper - lower) * value + lower;
};

auto operator<<(std::ostream& ostream, FourPointPairs const& four_point_pairs) -> std::ostream&
{
    ostream << "{src: [";
    for (auto const& point : four_point_pairs.src_points)
    {
        ostream << "{" << point[0] << ", " << point[1] << "}, ";
    }
    ostream << "], dst: [";
    for (auto const& point : four_point_pairs.dst_points)
    {
        ostream << "{" << point[0] << ", " << point[1] << "}, ";
    }
    ostream << "]}";
    return ostream;
}

auto operator<<(std::ostream& ostream, PointPairs const& point_pairs) -> std::ostream&
{
    ostream << "{src: [";
    for (auto const& point : point_pairs.src_points)
    {
        ostream << "{" << point[0] << ", " << point[1] << "}, ";
    }
    ostream << "], dst: [";
    for (auto const& point : point_pairs.dst_points)
    {
        ostream << "{" << point[0] << ", " << point[1] << "}, ";
    }
    ostream << "]}";
    return ostream;
}

auto operator<<(std::ostream& ostream, Points const& points) -> std::ostream&
{
    ostream << "[";
    for (auto const& point : points.points)
    {
        ostream << "{" << point(0) << ", " << point(1) << "}, ";
    }
    ostream << "]";
    return ostream;
}

auto generate_random_four_point_pairs(int const dataset_size) -> std::vector<FourPointPairs>
{
    // Create random number generators.
    std::random_device random_device;
    std::mt19937 engine(random_device());
    std::uniform_real_distribution<> distribution(-1, 1);

    std::vector<FourPointPairs> output_pairs(dataset_size);
    for (auto&& four_point_pairs : output_pairs)
    {
        for (auto&& point : four_point_pairs.src_points)
        {
            point << distribution(engine), distribution(engine);
        }
        for (auto&& point : four_point_pairs.dst_points)
        {
            point << distribution(engine), distribution(engine);
        }
    }

    return output_pairs;
}

auto generate_random_collinear_four_point_pairs(int const dataset_size)
    -> std::vector<FourPointPairs>
{
    // Create random number generators.
    std::random_device random_device;
    std::mt19937 engine(random_device());
    std::uniform_real_distribution<> distribution(-1, 1);

    // Generate random collinear points for both source and destination points.
    auto const src_collinear_points = generate_random_collinear_three_points(dataset_size);
    auto const dst_collinear_points = generate_random_collinear_three_points(dataset_size);

    std::vector<FourPointPairs> output_pairs(dataset_size);
    for (int i_pairs = 0; i_pairs < output_pairs.size(); ++i_pairs)
    {
        auto& four_point_pairs = output_pairs[i_pairs];

        // Put the collinear three points to the four point pairs.
        for (int i = 0; i < src_collinear_points[i_pairs].points.size(); ++i)
        {
            four_point_pairs.src_points[i] = src_collinear_points[i_pairs].points[i];
        }
        for (int i = 0; i < dst_collinear_points[i_pairs].points.size(); ++i)
        {
            four_point_pairs.dst_points[i] = dst_collinear_points[i_pairs].points[i];
        }

        // Generate the 4th point pair randomly.
        four_point_pairs.src_points[3] << distribution(engine), distribution(engine);
        four_point_pairs.dst_points[3] << distribution(engine), distribution(engine);
    }

    return output_pairs;
}

auto generate_random_point_pairs_of_coplanar_points_from_different_views(int const dataset_size,
                                                                         int const pair_count)
    -> std::vector<PointPairs>
{
    // Create random number generators.
    std::random_device random_device;
    std::mt19937 engine(random_device());
    std::uniform_real_distribution<> distribution(-1, 1);

    auto const pi = 2 * std::acos(0);

    std::vector<PointPairs> output_point_pairs(dataset_size);
    for (auto&& point_pairs : output_point_pairs)
    {
        // Reserve space for the output point pairs.
        point_pairs.src_points.reserve(pair_count);
        point_pairs.dst_points.reserve(pair_count);

        // Create random R3 coplanar points on plane z = 2.
        std::vector<Eigen::Vector3d> coplanar_points(pair_count);
        for (auto&& point : coplanar_points)
        {
            point << distribution(engine), distribution(engine), 2;
        }

        // Create two random rigid transformations.
        std::array<Eigen::Isometry3d, 2> isometries;
        for (auto&& isometry : isometries)
        {
            Eigen::Translation3d const translation(distribution(engine), distribution(engine),
                                                   distribution(engine));
            auto const axis =
                Eigen::Vector3d(distribution(engine), distribution(engine), distribution(engine))
                    .normalized();
            auto const angle = distribution(engine) * pi;

            // An isometry rotates first and then translates.
            isometry = translation * Eigen::AngleAxisd(angle, axis);
        }

        // Transform the coplanar points and project to R2.
        for (auto const& original_point : coplanar_points)
        {
            // Apply transformations to an original coplanar point.
            // The point is transformed to a pair of points in R3.
            auto const transformed_point_0 = isometries[0] * original_point;
            auto const transformed_point_1 = isometries[1] * original_point;

            // Skip the point if any of the two transformed point does not have
            // positive z coordinate.
            if (transformed_point_0(2) <= 0 || transformed_point_1(2) <= 0)
            {
                continue;
            }

            // Project the transformed points from R3 to R2, and store them as
            // an output point pair.
            point_pairs.src_points.emplace_back(transformed_point_0(0) / transformed_point_0(2),
                                                transformed_point_0(1) / transformed_point_0(2));
            point_pairs.dst_points.emplace_back(transformed_point_1(0) / transformed_point_1(2),
                                                transformed_point_1(1) / transformed_point_1(2));
        }
    }

    // Output the point pairs.
    return output_point_pairs;
}

auto generate_random_collinear_three_points(int const dataset_size) -> std::vector<Points>
{
    // Create random number generators.
    std::random_device random_device;
    std::mt19937 engine(random_device());
    std::uniform_real_distribution<> distribution(-1, 1);
    std::uniform_real_distribution<> ratio_distribution(-1, 2);

    std::vector<Points> output_vector_of_points(dataset_size);
    for (auto&& output_points : output_vector_of_points)
    {
        auto& points = output_points.points;
        points.resize(3);
        points[0] << distribution(engine), distribution(engine);
        points[1] << distribution(engine), distribution(engine);
        points[2] = points[0] + ratio_distribution(engine) * (points[1] - points[0]);
    }
    return output_vector_of_points;
}

std::vector<Points> generate_random_non_collinear_three_points(int const dataset_size)
{
    // Create random number generators.
    std::random_device random_device;
    std::mt19937 engine(random_device());
    std::uniform_real_distribution<> distribution(0, 1);

    auto const pi = 2 * std::acos(0);

    // Create the output vector.
    std::vector<Points> output_vector_of_points(dataset_size);
    for (auto&& output_points : output_vector_of_points)
    {
        // Center of a circle.
        Eigen::Vector2d center;
        center << scale_from_range_0_1(distribution(engine), -1, 1),
            scale_from_range_0_1(distribution(engine), -1, 1);

        // Radius of a circle.
        auto const radius = distribution(engine);

        // Create three points on the circle at non-duplicate random angles.
        auto& points = output_points.points;
        points.resize(3);
        auto angle = 0.0; // The value will be accumulated so define here.
        for (auto&& point : points)
        {
            // Add the angle at most a third of a round. Also do not add zero.
            // The choice of angle here avoids generating duplicate points.
            angle += scale_from_range_0_1(distribution(engine), 1e-5, 2.0 / 3.0 * pi);

            Eigen::Vector2d offset; // Offset from center.
            offset << radius * std::cos(angle), radius * std::sin(angle);
            point = center + offset;
        }
    }

    return output_vector_of_points;
}

auto generate_random_points_in_random_ranges(int const dataset_size,
                                             int const range_exponent_lower_bound,
                                             int const range_exponent_upper_bound,
                                             int const points_size_upper_bound)
    -> std::vector<Points>
{
    // Create random number generators.
    std::random_device random_device;
    std::mt19937 engine(random_device());
    // For generating random exponent at base of 10.
    std::uniform_int_distribution<> exponent_distribution(range_exponent_lower_bound,
                                                          range_exponent_upper_bound);
    std::uniform_int_distribution<> size_distribution(0, points_size_upper_bound);
    std::uniform_real_distribution<> real_distribution(0, 1); // Range [0, 1)

    // A function returning a random number with random base-10 exponent.
    auto get_random_value_at_random_exponent = [&real_distribution, &exponent_distribution,
                                                &engine]() {
        return real_distribution(engine) * std::pow(10, exponent_distribution(engine));
    };

    // A function returning -1 or 1 randomly.
    auto get_random_plus_minus_one = [&real_distribution, &engine]() {
        return real_distribution(engine) > 0.5 ? 1 : -1;
    };

    // Create output points.
    std::vector<Points> output_vector_of_points;
    output_vector_of_points.resize(dataset_size);
    for (auto&& output_points : output_vector_of_points)
    {
        // Create a random amount of empty points.
        auto&& points = output_points.points;
        points.resize(size_distribution(engine));

        // Randomly determine the range of the points.
        auto const [x_lower, x_upper] =
            std::minmax(get_random_plus_minus_one() * get_random_value_at_random_exponent(),
                        get_random_plus_minus_one() * get_random_value_at_random_exponent());
        auto const [y_lower, y_upper] =
            std::minmax(get_random_plus_minus_one() * get_random_value_at_random_exponent(),
                        get_random_plus_minus_one() * get_random_value_at_random_exponent());

        // Generate random values in the above ranges for the output points.
        for (auto&& point : points)
        {
            point << scale_from_range_0_1(real_distribution(engine), x_lower, x_upper),
                scale_from_range_0_1(real_distribution(engine), y_lower, y_upper);
        }
    }

    return output_vector_of_points;
}

auto check_collinear_three_points(Eigen::Vector2d const& point_0, Eigen::Vector2d const& point_1,
                                  Eigen::Vector2d const& point_2, Eigen::Vector2d const& point_3)
    -> bool
{
    return arcv::check_collinearity(point_0, point_1, point_2) ||
           arcv::check_collinearity(point_0, point_1, point_3) ||
           arcv::check_collinearity(point_0, point_2, point_3) ||
           arcv::check_collinearity(point_1, point_2, point_3);
}

template <class Iterator>
void verify_homography(Eigen::Matrix3d const& homography, Iterator src_points_begin,
                       Iterator const src_points_end, Iterator dst_points_begin,
                       Iterator const dst_points_end)
{
    // Check if the homography is invertible.
    // If a matrix (linear transformation) of homogeneous coordinates is invertible,
    // it is a homography.
    Eigen::Matrix3d homography_inverse;
    auto invertible = false;
    homography.computeInverseWithCheck(homography_inverse, invertible);
    BOOST_REQUIRE(invertible);

    // Check if the source points are mapped to the destination points by the
    // homography, and vice versa.
    while (src_points_begin != src_points_end && dst_points_begin != dst_points_end)
    {
        // Get a pair of points.
        auto const& src_point = *src_points_begin;
        auto const& dst_point = *dst_points_begin;

        // Map the source point and the destination point by the homography.
        Eigen::Vector3d mapped_src_point =
            homography * Eigen::Vector3d(src_point(0), src_point(1), 1);
        Eigen::Vector3d mapped_dst_point =
            homography_inverse * Eigen::Vector3d(dst_point(0), dst_point(1), 1);

        // The third coordinate of the mapped points should not be zero, or
        // they are points at infinity.
        BOOST_REQUIRE_NE(mapped_src_point(2), 0);
        BOOST_REQUIRE_NE(mapped_dst_point(2), 0);

        // Divide coordinates by the z component to make them inhomogeneous.
        mapped_src_point /= mapped_src_point(2);
        mapped_dst_point /= mapped_dst_point(2);

        // Check equality with the corresponding points.
        // Use BOOST_TEST to apply the specified tolerance of the test case.
        for (int i = 0; i < 2; ++i)
        {
            BOOST_TEST(mapped_src_point(i) == dst_point(i));
            BOOST_TEST(mapped_dst_point(i) == src_point(i));
        }

        // Move to the next point pair.
        ++src_points_begin;
        ++dst_points_begin;
    }
}

// Instantiate the function template.
template void verify_homography(Eigen::Matrix3d const& homography,
                                std::array<Eigen::Vector2d, 4>::iterator src_points_begin,
                                std::array<Eigen::Vector2d, 4>::iterator src_points_end,
                                std::array<Eigen::Vector2d, 4>::iterator dst_points_begin,
                                std::array<Eigen::Vector2d, 4>::iterator dst_points_end);

// Instantiate the function template.
template void verify_homography(Eigen::Matrix3d const& homography,
                                std::vector<Eigen::Vector2d>::iterator src_points_begin,
                                std::vector<Eigen::Vector2d>::iterator src_points_end,
                                std::vector<Eigen::Vector2d>::iterator dst_points_begin,
                                std::vector<Eigen::Vector2d>::iterator dst_points_end);

} // namespace test

} // namespace arcv

/**
 * Create a test suite with a floating point tolerance decorator.
 * Note that boost::unit_test::tolerance() only applies to BOOST_TEST(), but not
 * to other comparison macros like BOOST_CHECK_EQUAL().
 * The tolerance is tested with dataset size 1e5.
 */
BOOST_AUTO_TEST_SUITE(solve_homography_four_point_pairs, *boost::unit_test::tolerance(1e-3))

/**
 * In each test case, solve homography from four pairs of points and verify if
 * the homography maps between each point pair.
 */
BOOST_DATA_TEST_CASE(solve_homography_four_point_pairs,
                     boost::unit_test::data::make(arcv::test::generate_random_four_point_pairs(10)),
                     four_point_pairs)
{
    // Four point pairs.
    auto const& src_points = four_point_pairs.src_points;
    auto const& dst_points = four_point_pairs.dst_points;

    // Solve homography.
    Eigen::Matrix3d homography;
    // First check if any three input points are collinear.
    if (arcv::test::check_collinear_three_points(src_points[0], src_points[1], src_points[2],
                                                 src_points[3]) ||
        arcv::test::check_collinear_three_points(dst_points[0], dst_points[1], dst_points[2],
                                                 dst_points[3]))
    {
        // There are collinear points in the input points. An exception should
        // be thrown when solving homography. Terminate the test case.
        BOOST_REQUIRE_THROW(arcv::solve_homography(src_points, dst_points), std::invalid_argument);
        return;
    }
    else
    {
        // The input points are not collinear, and thus solving homography should
        // not throw any exception.
        BOOST_REQUIRE_NO_THROW(homography = arcv::solve_homography(src_points, dst_points));
    }

    // Verify the homography by mapping the source points and the destination points
    // and checking if the mapped points are equal to the corresponding points.
    arcv::test::verify_homography(homography, src_points.begin(), src_points.end(),
                                  dst_points.begin(), dst_points.end());
}

BOOST_AUTO_TEST_SUITE_END()

/**
 * For each test case, solve homography from four point pairs. There are collinear
 * points in the input points, and thus the homography solver should throw exception.
 */
BOOST_DATA_TEST_CASE(
    solve_homography_four_point_pairs_collinear,
    boost::unit_test::data::make(arcv::test::generate_random_collinear_four_point_pairs(10)),
    four_point_pairs)
{
    // The input point pairs contain collinear points. An exception should be
    // thrown when solving homography.
    BOOST_REQUIRE_THROW(
        arcv::solve_homography(four_point_pairs.src_points, four_point_pairs.dst_points),
        std::invalid_argument);
}

/**
 * The tolerance is tested with dataset size 1e5, pair count 1e2.
 */
BOOST_AUTO_TEST_SUITE(solve_homography_four_point_pairs_projected_from_r3_coplanar_points,
                      *boost::unit_test::tolerance(1e-5))

/**
 * For each test case, project random R3 coplanar points from two different views
 * to get pairs of R2 points. Solve homography from the first four point pairs,
 * and verify the homography by all point pairs.
 * If there are collinear points in the input points, simply skip the test case.
 */
BOOST_DATA_TEST_CASE(
    solve_homography_four_point_pairs_projected_from_r3_coplanar_points,
    boost::unit_test::data::make(
        arcv::test::generate_random_point_pairs_of_coplanar_points_from_different_views(10, 100)),
    point_pairs)
{
    // Check if there are enough points.
    if (point_pairs.src_points.size() < 4 || point_pairs.dst_points.size() < 4)
    {
        return;
    }

    auto const& src_points = point_pairs.src_points;
    auto const& dst_points = point_pairs.dst_points;

    // Solve homography from the first four point pairs.
    Eigen::Matrix3d homography;
    // First check if any three input points are collinear.
    if (arcv::test::check_collinear_three_points(src_points[0], src_points[1], src_points[2],
                                                 src_points[3]) ||
        arcv::test::check_collinear_three_points(dst_points[0], dst_points[1], dst_points[2],
                                                 dst_points[3]))
    {
        // There are collinear points in the input points. An exception should
        // be thrown when solving homography. Terminate the test case.
        BOOST_REQUIRE_THROW(
            arcv::solve_homography({src_points[0], src_points[1], src_points[2], src_points[3]},
                                   {dst_points[0], dst_points[1], dst_points[2], dst_points[3]}),
            std::invalid_argument);
        return;
    }
    else
    {
        // The input points are not collinear, and thus solving homography should
        // not throw any exception.
        BOOST_REQUIRE_NO_THROW(homography = arcv::solve_homography(
                                   {src_points[0], src_points[1], src_points[2], src_points[3]},
                                   {dst_points[0], dst_points[1], dst_points[2], dst_points[3]}));
    }

    // Verify the homography by mapping all source points and destination points
    // and checking if the mapped points are equal to the corresponding points.
    arcv::test::verify_homography(homography, point_pairs.src_points.begin(),
                                  point_pairs.src_points.end(), point_pairs.dst_points.begin(),
                                  point_pairs.dst_points.end());
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(check_collinearity)

BOOST_DATA_TEST_CASE(
    check_collinear_three_points,
    boost::unit_test::data::make(arcv::test::generate_random_collinear_three_points(10)),
    input_points)
{
    // Check if the input points are collinear.
    auto const& points = input_points.points;
    BOOST_TEST(arcv::check_collinearity(points[0], points[1], points[2]));

    // Check if duplicate three points are collinear.
    BOOST_TEST(arcv::check_collinearity(points[0], points[0], points[2]));
    BOOST_TEST(arcv::check_collinearity(points[0], points[1], points[1]));
    BOOST_TEST(arcv::check_collinearity(points[2], points[1], points[2]));
    BOOST_TEST(arcv::check_collinearity(points[1], points[1], points[1]));
}

BOOST_DATA_TEST_CASE(
    check_non_collinear_three_points,
    boost::unit_test::data::make(arcv::test::generate_random_non_collinear_three_points(10)),
    input_points)
{
    // Check if the input points are not collinear.
    auto const& points = input_points.points;
    BOOST_TEST(!arcv::check_collinearity(points[0], points[1], points[2]));
}

BOOST_AUTO_TEST_SUITE_END()

// Tolerance 1e-11 is tested by dataset_size 1e5, points_size_upper_bound 1e2.
BOOST_AUTO_TEST_SUITE(normalize_range, *boost::unit_test::tolerance(1e-11))

// Assume there are at least a pair of input random points are different.
BOOST_DATA_TEST_CASE(normalize_range,
                     boost::unit_test::data::make(
                         arcv::test::generate_random_points_in_random_ranges(10, -15, 15, 1e1)),
                     input_points)
{
    // Normalize the range of the input points to x: [-1, 1], y: [-1, 1] in place.
    auto const normalized_points = arcv::normalize_range(input_points.points);

    if (normalized_points.empty())
    {
        // Nothing to check if there is no point.
        return;
    }
    else if (normalized_points.size() == 1)
    {
        // If there is only one point, the point should be at the origin.
        // Use BOOST_TEST to apply the specified tolerance of the test case.
        auto const& point = normalized_points[0];
        BOOST_TEST(point(0) == 0);
        BOOST_TEST(point(1) == 0);
        return;
    }

    // Find the x y range of the normalized points, and check if the range is
    // x: [-1, 1], y: [-1, 1].
    // Assume that at least two points have different x and y, which is reasonable
    // for multiple random input points.
    // Use BOOST_TEST to apply the specified tolerance of the test case.
    auto const [min_x_point_iterator, max_x_point_iterator] = std::minmax_element(
        normalized_points.begin(), normalized_points.end(),
        [](auto const& point_0, auto const& point_1) { return point_0(0) < point_1(0); });
    auto const [min_y_point_iterator, max_y_point_iterator] = std::minmax_element(
        normalized_points.begin(), normalized_points.end(),
        [](auto const& point_0, auto const& point_1) { return point_0(1) < point_1(1); });
    auto const x_min = (*min_x_point_iterator)(0);
    auto const x_max = (*max_x_point_iterator)(0);
    auto const y_min = (*min_y_point_iterator)(1);
    auto const y_max = (*max_y_point_iterator)(1);
    BOOST_TEST(x_min == -1);
    BOOST_TEST(x_max == 1);
    BOOST_TEST(y_min == -1);
    BOOST_TEST(y_max == 1);
}

BOOST_AUTO_TEST_SUITE_END()
